var currentYear = (new Date).getFullYear();
$(document).ready(function() {
	$("#year").text((new Date).getFullYear());
});
$(window).load(function() {
	$("#status").fadeOut();
	$("#preloader").delay(100).fadeOut("slow");
});

$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "fade",
    controlNav: false,
    directionNav: false,
  });
});
/*
$(document).bind("mobileinit", function() {
  $.mobile.ignoreContentEnabled = true;
});

*/
$(document).bind("mobileinit", function() {
	$.mobile.hashListeningEnabled = false;
	$.mobile.pushStateEnabled = false;
	$.mobile.ignoreContentEnabled = true;
});